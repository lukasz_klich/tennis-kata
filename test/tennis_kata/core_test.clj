(ns tennis-kata.core-test
  (:require [clojure.test :refer :all]
            [tennis-kata.core :refer :all]))

(deftest isGameOverTest
  (testing "If nobody won more than 4 balls isn't over"
    (is (not (isGameOver '(1 2))))
    (is (not (isGameOver '(3 2))))
    (is (not (isGameOver '(0 3)))))
  (testing "If the difference is lower then 2 isn't over"
    (is (not (isGameOver '(8 9))))
    (is (not (isGameOver '(4 3))))
    (is (not (isGameOver '(5 4)))))
  (testing "If won more then 4 balls and difference is at least 2"
    (is (isGameOver '(7 9)))
    (is (isGameOver '(5 3)))
    (is (isGameOver '(4 2)))))

(deftest convertScoreTest
  (testing "Test all possibilities"
    (is (= (convertScore '(1 1)) '(15 15)))
    (is (= (convertScore '(1 3)) '(15 40)))
    (is (= (convertScore '(2 0)) '(30 0)))
    (is (= (convertScore '(5 3)) '(500 40)))))

(deftest gameEndsWithAllPointForOnePlayer
  (testing "what the name says"
    (is (= (playTennis (fn [[x y]] (list (+ x 1) y)))
           '(4 0)))))
