(ns tennis-kata.core
  (:gen-class)
  (:use [clojure.string :only [join]]))

(defn nextService [[p1 p2]]
  (if (zero? (rand-int 2))
    (list (+ p1 1) p2)
    (list p1 (+ p2 1))))

(defn isGameOver [[p1 p2]]
  (cond (and (< p1 4)
             (< p2 4)) false
        (< (Math/abs (- p1 p2)) 2) false
        :default true))

(defn playTennis [serviceFunction]
  (defn innerPlayTennis [currentState]
    (if (isGameOver currentState) currentState
        (recur (serviceFunction currentState))))
  (innerPlayTennis '(0 0)))

(defn convertScore [[score1 score2]]
  (defn toPoints [i]
   (cond (= i 0) 0
         (= i 1) 15
         (= i 2) 30
         (= i 3) 40
         :default (* i 100)))
  (list (toPoints score1) (toPoints score2)))

(defn printScore [state]
  (println "Score is (" (join ", " (convertScore state)) ")"))

(defn -main
  [& args]
  (printScore (playTennis nextService)))
